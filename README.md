This is a simple project that shows you how to do some common redistricting queries.

1. run the docker_command.sh file, which will give you an isolated PostGIS database that you can use for this.
2. run fetch_census.sh to get all the census data
3. run load_psql.sh to load them all into data

The sample plan for which we want all the queries is called "ca_five_districts". 

Thanks!
